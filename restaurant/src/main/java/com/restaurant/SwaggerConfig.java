package com.restaurant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.*;

@Configuration
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

    @Value("${spring.security.oauth2.resourceserver.jwt.issuer-uri}")
    private String issuerUrl;

    @Value("${swagger.host}")
    private String host;

    @Value("${swagger.protocol}")
    private String protocol;

    @Bean
    public Docket api() {
        Contact contact = new Contact(
                "Demo Creator",
                "https://restaurant.widemo.xyz",
                "noreply@widemo.xyz"
        );
        List<VendorExtension> vendorExtensions = new ArrayList<>();

        ApiInfo apiInfo = new ApiInfo(
                "RESTaurant Menu",
                "Sample API for WI scanning, showing both Basic and OAuth authentication.", "1.0",
                "https://restaurant.widemo.xyz", contact,
                "None", "https://restaurant.widemo.xyz",vendorExtensions);


        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .host(host)
                .protocols(Collections.singleton(protocol))
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.restaurant"))
                .paths(PathSelectors.any())
                .build()
                .securityContexts(Arrays.asList(securityContext()))
                .securitySchemes(Arrays.asList(basicAuthScheme(), oAuth2AccessCodeScheme(), oAuth2ImplicitScheme(), oAuth2PasswordScheme(), oAuth2ClientCredentialScheme()))
                .useDefaultResponseMessages(false);
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(Arrays.asList(basicAuthReference(), oAuth2AccessCodeReference(), oAuth2ImplicitReference(), oAuth2PasswordReference(), oAuth2ClientCredentialsReference()))
                .build();
    }

    private SecurityScheme basicAuthScheme() {
        return new BasicAuth("basicAuth");
    }

    private SecurityReference basicAuthReference() {
        return new SecurityReference("basicAuth", new AuthorizationScope[0]);
    }

    private SecurityScheme oAuth2AccessCodeScheme() {
        TokenRequestEndpoint tokenRequestEndpoint = new TokenRequestEndpoint(issuerUrl + "/protocol/openid-connect/auth", "client_id", "client_secret");
        TokenEndpoint tokenEndpoint = new TokenEndpoint(issuerUrl + "/protocol/openid-connect/token", "access_token");
        GrantType grant = new AuthorizationCodeGrant(tokenRequestEndpoint, tokenEndpoint);
        return new OAuth("oAuth2AccessCode", Collections.emptyList(), Arrays.asList(grant));
    }

    private SecurityReference oAuth2AccessCodeReference() {
        return new SecurityReference("oAuth2AccessCode", new AuthorizationScope[0]);
    }

    private SecurityScheme oAuth2ImplicitScheme() {
        LoginEndpoint loginEndpoint = new LoginEndpoint(issuerUrl + "/protocol/openid-connect/auth");
        GrantType grant = new ImplicitGrant(loginEndpoint, "access_token");
        return new OAuth("oAuth2Implicit", Collections.emptyList(), Arrays.asList(grant));
    }

    private SecurityReference oAuth2ImplicitReference() {
        return new SecurityReference("oAuth2Implicit", new AuthorizationScope[0]);
    }

    private SecurityScheme oAuth2PasswordScheme() {
        GrantType grant = new ResourceOwnerPasswordCredentialsGrant(issuerUrl + "/protocol/openid-connect/token");
        return new OAuth("oAuth2Password", Collections.emptyList(), Arrays.asList(grant));
    }

    private SecurityReference oAuth2PasswordReference() {
        return new SecurityReference("oAuth2Password", new AuthorizationScope[0]);
    }


    private SecurityScheme oAuth2ClientCredentialScheme() {
        GrantType grant = new ClientCredentialsGrant(issuerUrl + "/protocol/openid-connect/token");
        return new OAuth("oAuth2ClientCredentials", Collections.emptyList(), Arrays.asList(grant));
    }

    private SecurityReference oAuth2ClientCredentialsReference() {
        return new SecurityReference("oAuth2ClientCredentials", new AuthorizationScope[0]);
    }
}
