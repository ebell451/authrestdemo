package com.restaurant;

import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

@RestController
@Api(tags = { "menu" }, value = "API root for restaurant menu functions.")
@io.swagger.annotations.ApiResponses(value = {
        @ApiResponse(code = 401, message = "Unauthorized. Provide HTTP Basic credentials or an OAuth2 token.", response = void.class),
        @ApiResponse(code = 500, message = "Internal server error.", response = void.class) })
public class MenuController {

    private final Map<Type, Deque<Food>> menu = new HashMap<>();

    {
        menu.put(Type.EatIn, new ArrayDeque<>());
        menu.put(Type.TakeOut, new ArrayDeque<>());

        menu.get(Type.EatIn).add(new Food("steak", new BigDecimal("20.00")));
        menu.get(Type.EatIn).add(new Food("pizza", new BigDecimal("16.00")));
        menu.get(Type.TakeOut).add(new Food("pizza", new BigDecimal("13.00")));
    }

    @RequestMapping(value = "/menu/{type}", method = RequestMethod.GET)
    public Collection<Food> get(@ApiParam(
            value = "Selects between the Eat-In and Take-Out menus",
            example = "EatIn") @PathVariable Type type) {
        return menu.get(type);
    }

    @RequestMapping(value = "/menu/{type}", method = RequestMethod.POST)
    public void add(@ApiParam(
            value = "Selects between the Eat-In and Take-Out menus",
            example = "EatIn") @PathVariable Type type, @RequestBody Food food) {
        menu.get(type).add(food);
        if(menu.get(type).size() > 10) menu.get(type).remove();
    }


    @ApiModel
    public enum Type {
        EatIn, TakeOut;
    }

    @ApiModel(description = "Represents a food item on the menu.")
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Food {
        @ApiModelProperty(example = "hamburger", required = true)
        String name;
        @ApiModelProperty(example = "12.34", required = true)
        BigDecimal price;
    }

}
