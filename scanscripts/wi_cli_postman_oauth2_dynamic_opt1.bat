curl -X POST -sS ^
  -H "Content-type: application/x-www-form-urlencoded" ^
  -H "Accept: application/json" ^
  -H "Authorization: Basic cmVzdGF1cmFudDpmMGFkNzUyYy00YmU3LTQ4MTktYjA2Mi1kNGI1NTA1N2M4MmU=" ^
  -d "grant_type=password&username=user1&password=pwd" ^
  https://keycloak.widemo.xyz/auth/realms/demorealm/protocol/openid-connect/token ^
  | jq -r .access_token ^
  >token.tmp

@for /f "delims=" %%x in (token.tmp) do @set TOKEN=%%x
jq ".auth.bearer[0].value = \"%TOKEN%\"" restaurant_postman_bearer.json >restaurant_postman_bearer.tmp.json

wi.exe -v -n "RESTaurant CLI-started Postman OAuth2 Dynamic - Option 1" ^
  -ah https://restaurant.widemo.xyz ^
  -ps 1 ^
  -pdac ^
  -plc restaurant_postman_gettoken.json ^
  -pwc restaurant_postman_bearer.tmp.json ^
  -rs @BearerResponseStateRule.txt ^
  -pls [STATUSCODE]401

@erase *.tmp
