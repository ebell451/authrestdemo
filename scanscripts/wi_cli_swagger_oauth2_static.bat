curl -X POST -sS ^
  -H "Content-type: application/x-www-form-urlencoded" ^
  -H "Accept: application/json" ^
  -H "Authorization: Basic cmVzdGF1cmFudDpmMGFkNzUyYy00YmU3LTQ4MTktYjA2Mi1kNGI1NTA1N2M4MmU=" ^
  -d "grant_type=password&username=user1&password=pwd" ^
  https://keycloak.widemo.xyz/auth/realms/demorealm/protocol/openid-connect/token ^
  | jq -r .access_token ^
  >token.tmp

@for /f "delims=" %%x in (token.tmp) do @set TOKEN=%%x
wi.exe -v -n "RESTaurant CLI-started Swagger OAuth2 Static" ^
  -u https://restaurant.widemo.xyz/v2/api-docs -api swagger ^
  -at "Bearer %TOKEN%"

@erase *.tmp
