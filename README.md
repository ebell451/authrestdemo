# authrestdemo

This a sample REST application (the RESTaurant) with authentication, designed to demonstrate authenticated REST-API scanning. It supports both Basic and Oauth2 authentication. 

The application has been deployed for testing purposes, with https://restaurant.widemo.xyz being the resource server and https://keycloak.widemo.xyz being the authorization server.

### Swagger
* Swagger-UI page: https://restaurant.widemo.xyz/swagger-ui/
* Swagger APIdoc:  https://restaurant.widemo.xyz/v2/api-docs

### Basic Auth
* Username: user1
* Password: pwd

### OAuth2
All 4 standard OAuth2 flows/grant_types are supported, using the following parameters as required in each case:

* client id: restaurant
* client secret: f0ad752c-4be7-4819-b062-d4b55057c82e
* user id: user1
* user password: pwd
* scope: profile
* callback url: https://restaurant.widemo.xyz
* auth url: https://keycloak.widemo.xyz/auth/realms/demorealm/protocol/openid-connect/auth
* access token url: https://keycloak.widemo.xyz/auth/realms/demorealm/protocol/openid-connect/token
